using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FinishTrigger : MonoBehaviour
{
    private Action collisionEvent;
        
    public void SetCollisionEvent(Action onCollision)
    {
        collisionEvent = onCollision;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collisionEvent?.Invoke();
        }
    }
}
