using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckpointTrigger : MonoBehaviour
{
    private Action<CheckpointTrigger> collisionEvent;

    public void SetCollisionEvent(Action<CheckpointTrigger> onCollision)
    {
        collisionEvent = onCollision;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collisionEvent?.Invoke(this);
        }
    }
}
