using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    private Button levelButton1;
    [SerializeField]
    private Button levelButton2;
    [SerializeField]
    private Button levelButton3;
    [SerializeField]
    private Button controlsButton;
    [SerializeField]
    private Button exitButton;
    [SerializeField]
    private Button backButton;
    [SerializeField]
    private GameObject buttons;
    [SerializeField]
    private GameObject controls;



    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1; 
        SetControlsActive(false);
        levelButton1.onClick.AddListener(() => LoadLevel("Level_1"));
        levelButton2.onClick.AddListener(() => LoadLevel("Level_2"));
        levelButton3.onClick.AddListener(() => LoadLevel("Level_3"));
        controlsButton.onClick.AddListener(() => SetControlsActive(true));
        backButton.onClick.AddListener(() => SetControlsActive(false));
        exitButton.onClick.AddListener(Application.Quit);
    }

    private void SetControlsActive(bool value)
    {
        buttons.SetActive(!value);
        controls.SetActive(value);
    }

    private void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }

}
