using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    [SerializeField]
    private float accelerationSpeed = 25f;
    [SerializeField]
    private float rotationSpeed = 2f;
    [Range(0, 1)]
    [SerializeField]
    private float baseDriftFactor = 0.90f;
    [SerializeField]
    [Range(0, 1)]
    private float boostDriftFactor = 0.90f;
    [SerializeField]
    private float maxSpeed = 50f;
    [SerializeField]
    public Rigidbody2D carRigidbody;

    private float xInput;
    private float yInput;
    private float rotationValue = 0;
    private bool isDrifting = false;
    private float driftValue = 0;
    private Vector2 riverVector = Vector2.zero;
    public bool isInRiver = false;

    void Start()
    {
        rotationValue = transform.rotation.eulerAngles.z;
    }

    void Update()
    {
        xInput = Input.GetAxis("Horizontal");
        yInput = Input.GetAxis("Vertical");
        isDrifting = Input.GetKey(KeyCode.Space);
    }
    private void CalculateMovement()
    {
        float howMuchForward = Vector2.Dot(carRigidbody.velocity, transform.up);

        if (yInput > 0)
        {
            //don't brake if player is going forward
            carRigidbody.drag = 0;
            // max forward speed
            if (howMuchForward > maxSpeed || carRigidbody.velocity.sqrMagnitude > maxSpeed * maxSpeed)
                return;
        }
        else if (yInput < 0)
        {
            //frist rapidly brake before you start to back up
            if (howMuchForward > 0)
            {
                carRigidbody.drag = 10;
            }
            else
            {
                carRigidbody.drag = 0;
            }
            // max back up speed
            if (howMuchForward < -maxSpeed * 0.5f)
                return;
        }
        else if (yInput == 0)
        {
            // slow down if player is not pushing buttons
            carRigidbody.drag = Mathf.Lerp(carRigidbody.drag, 1.5f, Time.deltaTime * 6f);
        }
        Vector2 accelerationVector = transform.up * accelerationSpeed * yInput;
        carRigidbody.AddForce(accelerationVector);
    }

    public void GoThroughRiver(Collider2D collision)
    {
        var river = collision.GetComponent<RiverController>();
        if (isInRiver)
        {
            riverVector = river.RiverFactor * transform.right * Vector2.Dot(river.FlowDirectionToVector(), transform.right);
        }
        else
        {
            riverVector = Vector2.zero;
        }
    }

    private void FixedUpdate()
    {
        CalculateMovement();

        Vector2 forwardVelocity = transform.up * Vector2.Dot(carRigidbody.velocity, transform.up);
        Vector2 rightVelocity = transform.right * Vector2.Dot(carRigidbody.velocity, transform.right);
        driftValue = isDrifting ? boostDriftFactor : baseDriftFactor;

        carRigidbody.velocity = riverVector + forwardVelocity + rightVelocity * driftValue;

        //no turning in place
        float speedReqToTurn = carRigidbody.velocity.magnitude / 10f;
        speedReqToTurn = Mathf.Clamp01(speedReqToTurn);
        if (yInput < 0) xInput *= -1;
        rotationValue -= xInput * rotationSpeed * speedReqToTurn;

        carRigidbody.MoveRotation(rotationValue);
    }

}
