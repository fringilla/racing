using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI currentTimerLabel;
    [SerializeField]
    private TextMeshProUGUI bestTimerLabel;
    [SerializeField]
    private GameObject pausePanel;
    [SerializeField]
    private Button exitButton;
    [SerializeField]
    private Button backButton;
    [SerializeField]
    private Button resumeButton;

    private bool paused = false;


    private static InGameMenu instance;
    public static InGameMenu Instance
    { 
    get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<InGameMenu>();
            }
            return instance;
        }
    
    }
    private void Start()
    {
        SetPausePanel(false);
        exitButton.onClick.AddListener(() => Application.Quit());
        backButton.onClick.AddListener(() => SceneManager.LoadScene("MainMenu"));
        resumeButton.onClick.AddListener(() => SetPause());

    }
    public void SetCurrentTime(string time)
    {
        currentTimerLabel.text = time;
    }
    public void SetBestTime(string time)
    {
        bestTimerLabel.text = time;
    }
    private void SetPausePanel(bool value)
    {
        pausePanel.SetActive(value);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SetPause();
        }
    }

    private void SetPause()
    {
        Time.timeScale = paused ? 1 : 0;
        SetPausePanel(!paused);
        paused = !paused;
    }

}
