using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCollider : MonoBehaviour
{
    [SerializeField]
    private CarController carController;
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "River")
        {
            carController.isInRiver = true;
            carController.GoThroughRiver(collision);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "River")
        {
            carController.isInRiver = false;
            carController.GoThroughRiver(collision);
        }
    }
}
