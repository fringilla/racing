using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private List<CheckpointTrigger> checkpoints = new List<CheckpointTrigger>();
    private bool timerStarted = false;
    private float timer = 0;
    private int nextCheckpointIndex = 0;
    private float bestTime = float.MaxValue;

    void Start()
    {
       // finish.SetCollisionEvent(OnFinishCrossed);
        foreach (var checkpoint in checkpoints)
        {
            checkpoint.SetCollisionEvent(GoThroughCheckpoint);
            SetCheckpointCones(checkpoint, 0.8f);
        }
        SetCheckpointCones(checkpoints[0], 1.1f);
        timer = 0;
        bestTime = float.MaxValue;
        InGameMenu.Instance.SetCurrentTime(FromSeconds(0));
        InGameMenu.Instance.SetBestTime(FromSeconds(0));

    }

    void Update()
    {
        if (timerStarted)
        {
            timer += Time.deltaTime;
            InGameMenu.Instance.SetCurrentTime(FromSeconds(timer));
        }
    }

    private void GoThroughCheckpoint(CheckpointTrigger trigger)
    {
        if (checkpoints.IndexOf(trigger) == nextCheckpointIndex)
        {
            if (trigger.transform.parent.tag == "finish")
            {
                OnFinishCrossed();
            }
            SetCheckpointCones(trigger, 0.8f);
            nextCheckpointIndex = (nextCheckpointIndex + 1) % checkpoints.Count;
            var nextTrigger = checkpoints[nextCheckpointIndex];
            SetCheckpointCones(nextTrigger, 1.1f);
        }
        timerStarted = true;
    }

    private void SetCheckpointCones(CheckpointTrigger trigger, float scale)
    {
        foreach (Transform cone in trigger.transform)
        {
            cone.localScale = Vector3.one * scale;
        }
    }
    private void OnFinishCrossed()
    {
        if (timerStarted)
        {
            if (timer < bestTime)
            {
                bestTime = timer;
                InGameMenu.Instance.SetBestTime(FromSeconds(bestTime));
            }
            timer = 0;
        }
    }
    private static string FromSeconds(float seconds)
    {
        TimeSpan time = TimeSpan.FromSeconds(seconds);
        string str = "";
        if (time.TotalHours >= 1)
        {
            str += time.ToString("hh") + ":";
        }
        if (time.TotalMinutes >= 1)
        {
            str += time.ToString("mm") + ":";
        }
        return str + time.ToString("ss\\.fff");
    }

}
