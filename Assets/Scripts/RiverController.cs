using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EFlowDirection
{ 
    NORTH,
    SOUTH,
    EAST,
    WEST,
}

public class RiverController : MonoBehaviour
{
    [SerializeField]
    private float riverFactor;
    [SerializeField]
    private EFlowDirection flowDirection;
    public float RiverFactor { get { return riverFactor; } }
    
    private void Start()
    {
        foreach(Transform ch in transform)
        {
            switch (flowDirection)
            {
                case EFlowDirection.NORTH:
                    ch.localRotation = Quaternion.Euler(0, 0, 90);
                    break;
                case EFlowDirection.SOUTH:
                    ch.localRotation = Quaternion.Euler(0, 0, -90);
                    break;
                case EFlowDirection.EAST:
                    ch.localRotation = Quaternion.Euler(0, 0, 0);
                    break;
                case EFlowDirection.WEST:
                    ch.localRotation = Quaternion.Euler(0, 0, 180);
                    break;
            }

        }
    }
    public Vector2 FlowDirectionToVector()
    {
        return flowDirection switch
        {
            EFlowDirection.NORTH => Vector2.up,
            EFlowDirection.SOUTH => Vector2.down,
            EFlowDirection.EAST => Vector2.right,
            EFlowDirection.WEST => Vector2.left,
            _ => Vector2.zero,
        };
    }   
}
